#!/usr/bin/env python

from __future__ import division
import rospy
from gazebo_msgs.srv import GetModelState
from gazebo_msgs.srv import GetLinkState
from gazebo_msgs.srv import SetModelState
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import LinkState
from labrider_lib.srv import HeatStatus
from std_srvs.srv import Trigger, TriggerRequest, TriggerResponse
from labrider_arena.srv import SingleHeat
import math
import sys
from std_msgs.msg import String
from angles import normalize_angle
import time
import os
import requests

class MapArr(object):

    def __init__(self, w, h):
        self.width = w
        self.height = h
        self.sleep_time = 0.2

        self.map = [[-1000]*h for x in range(w)]
        self.heat_map = []

        self.checkpoint_1_count = 0
        self.checkpoint_1_flag = False

        self.checkpoint_3_count = 0
        self.checkpoint_3_flag = False

        self.checkpoint_4_count = 0
        self.checkpoint_4_flag = False


        self.initialize_services()

    def initialize_services(self):
        try:
            rospy.wait_for_service("/return_all_heat", 5.0)
            self.get_heats = rospy.ServiceProxy('return_all_heat', Trigger)
            resp = self.get_heats()

            map_heats = resp.message.split(":")

            map_heats.remove("")

            self.heat_map = list(map(int, map_heats))

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/process_single_heat", 5.0)
            self.spawn_single_model = rospy.ServiceProxy('process_single_heat', SingleHeat)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/clean_single_tile", 5.0)
            self.clean_single_model = rospy.ServiceProxy('clean_single_tile', SingleHeat)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))


    def __getitem__(self, key):
        return self.get_value(key[0], key[1])

    def __setitem__(self, key, value):
        if self.map[key[0]][key[1]] == value:
            pass
        else:
            if self.get_value(key[0], key[1]) == -1 and not value == -1:
                pass
            else:
                self.clean_tile(key[0], key[1])
                self.set_value(key[0], key[1], value)
                self.map[key[0]][key[1]] = value
        

        #checkpoints
        # first checkpoint
        if key[0] == 11 and value == -1 and not self.checkpoint_1_flag:
            self.checkpoint_1_count += 1

        if key[0] == 10 and key[1] == 8 and value == 0 and not self.checkpoint_1_flag:
            self.checkpoint_1_count += 1

        if not self.checkpoint_1_flag and self.checkpoint_1_count == 2:
            self.checkpoint_1_flag = True
            #print("first")
            self.achieve(82)

        # second checkpoint
        if key[0] == 2 and key[1] == 4 and value == 1:
            #print("second")
            self.achieve(83)

        # third checkpoint
        if key[0] == 1 and key[1] == 4 and value == 2 and not self.checkpoint_3_flag:
            self.checkpoint_3_count += 1

        if key[0] == 3 and key[1] == 4 and value == 2 and not self.checkpoint_3_flag:
            self.checkpoint_3_count += 1

        if not self.checkpoint_3_flag and self.checkpoint_3_count == 2:
            self.checkpoint_3_flag = True
            #print("third")
            self.achieve(84)

        # fourth checkpoint
        if key[0] == 1 and key[1] == 2 and value == 4 and not self.checkpoint_4_flag:
            self.checkpoint_4_count += 1

        if key[0] == 1 and key[1] == 1 and value == 5 and not self.checkpoint_4_flag:
            self.checkpoint_4_count += 1

        if not self.checkpoint_4_flag and self.checkpoint_4_count == 2:
            self.checkpoint_4_flag = True
            #print("fourth")
            self.achieve(85)


    def get_map_heats(self):
        resp = self.get_heats()

        map_heats = resp.message.split(":")
        map_heats.remove("")
        self.heat_map = list(map(int, map_heats))


    def set_value(self, x, y, number):
        number_str = str(number)

        resp = self.spawn_single_model(x,y,number_str)


    def get_value(self, x, y):
        self.get_map_heats()
        return self.heat_map[y*self.width + x]


    def clean_tile(self, x, y):
        resp = self.clean_single_model(x,y,"")


    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


class Map:

    def __init__(self):
        self.width = 12
        self.height = 10
        
        self.point_a = [1, 1]
        self.point_b = [2, 4]
        self.point_c = [7, 8]
        self.point_d = [10, 4]

        self.startX = self.point_a[0]
        self.startY = self.point_a[1]

        self.finishX = self.point_b[0]
        self.finishY = self.point_b[1]

        self.value = MapArr(self.width, self.height)

        s = rospy.Service('/change_finish', Trigger, self.change_finish)


    def change_finish(self, req):
        if self.finishX == self.point_b[0]:
            self.finishX = self.point_c[0]
            self.finishY = self.point_c[1]

            self.startX = self.point_b[0]
            self.startY = self.point_b[1]

        elif self.finishX == self.point_c[0]:
            self.finishX = self.point_d[0]
            self.finishY = self.point_d[1]

            self.startX = self.point_c[0]
            self.startY = self.point_c[1]

        elif self.finishX == self.point_d[0]:
            self.startX = self.point_d[0]
            self.startY = self.point_d[1]

        return TriggerResponse(
            success = True,
            message = ""
        )

    def blocked(self, x, y):
        if self.value.heat_map[y*self.width + x] == -1:
            return True
        else:
            return False
