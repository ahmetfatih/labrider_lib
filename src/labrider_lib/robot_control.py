#!/usr/bin/env python

from __future__ import division
import rospy
from conversions import Vector3, Quaternion, EulerAngles
from gazebo_msgs.srv import GetLinkState
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import LinkState
from std_srvs.srv import Trigger, TriggerRequest
import math
import sys
from std_msgs.msg import String
from angles import normalize_angle
import time

class RobotControl:

    def __init__(self, robot_data, initial_pos, event_data):

        self.robot_name = robot_data['robot_name']
        self.base_link = robot_data['base_link']
        self.left_wheels = robot_data['left_wheels']
        self.right_wheels = robot_data['right_wheels']
        self.wheel_pos = math.sqrt(robot_data['wheel_pos'][0] * robot_data['wheel_pos'][1])

        # N=>pi/2 E=>0 S=>3*pi/2 W=>pi
        self.dir = initial_pos['yaw']
        self.pos_x = initial_pos['x']
        self.pos_y = initial_pos['y']
        self.pos_z = initial_pos['z']

        self.prev_dir = self.dir
        self.prev_pos_x = self.pos_x
        self.prev_pos_y = self.pos_y

        self.unit_length = event_data['unit_length']
        self.event_period = event_data['event_period']
        self.msg_rate = 100

        self.set_model_state = rospy.Publisher("/gazebo/set_model_state", ModelState, queue_size=10)
        self.set_link_state = rospy.Publisher("/gazebo/set_link_state", LinkState, queue_size=10)

        self.heat_map = []
        self.width = 12

        time.sleep(1)
        self.update(self.pos_x, self.pos_y, self.dir)


    def update(self, x, y, heading):
        q = Quaternion()
        q.euler_to_quaternion(0, 0, heading)

        state_msg = ModelState()
        state_msg.model_name = self.robot_name
        state_msg.pose.position.x = x
        state_msg.pose.position.y = y
        state_msg.pose.position.z = self.pos_z
        state_msg.pose.orientation.x = q.x
        state_msg.pose.orientation.y = q.y
        state_msg.pose.orientation.z = q.z
        state_msg.pose.orientation.w = q.w
        self.set_model_state.publish(state_msg)

    def update_wheel(self, link, radius, distance):

        def get_link_state(link):
            rospy.wait_for_service('/gazebo/get_link_state')
            try:
                get_link_state = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState)
                ls = get_link_state(link, self.base_link)

                link_pose = Vector3(ls.link_state.pose.position.x, ls.link_state.pose.position.y, ls.link_state.pose.position.z)
                ql = Quaternion(ls.link_state.pose.orientation.x, ls.link_state.pose.orientation.y, ls.link_state.pose.orientation.z, ls.link_state.pose.orientation.w)
                link_rotation = EulerAngles()
                link_rotation.quaternion_to_euler_angles(ql)

                return link_pose, link_rotation
            except rospy.ServiceException, e:
                print "Service call failed: %s"%e

        angle =  distance / (2 * radius)
        prev_pose, prev_rot = get_link_state(link)

        next_angle = normalize_angle(prev_rot.pitch) + angle

        if next_angle >= math.pi/2:
            next_angle = next_angle - math.pi
        if next_angle <= -math.pi/2:
            next_angle = next_angle + math.pi

        q = Quaternion()
        q.euler_to_quaternion(prev_rot.roll, next_angle, prev_rot.yaw)

        link_msg = LinkState()
        link_msg.link_name = link
        link_msg.reference_frame = self.base_link
        link_msg.pose.position.x = prev_pose.x
        link_msg.pose.position.y = prev_pose.y
        link_msg.pose.position.z = prev_pose.z
        link_msg.pose.orientation.x = q.x
        link_msg.pose.orientation.y = q.y
        link_msg.pose.orientation.z = q.z
        link_msg.pose.orientation.w = q.w
        self.set_link_state.publish(link_msg)

    def polynomial_coef(self, initial_pos, final_pos, final_time, initial_vel = 0, final_vel = 0):

        a0 = initial_pos
        a1 = initial_vel
        a2 = (3/(final_time**2))*(final_pos - initial_pos) - (2/final_time)*initial_vel - final_vel/final_time
        a3 = -(2/(final_time**3))*(final_pos - initial_pos) +  (final_vel - initial_vel)/final_time**2

        return a0, a1, a2, a3

    def pos_on_t(self, t, a0, a1, a2, a3):

        pos = a3*t**3 + a2*t**2 + a1*t + a0;
        vel = 3*a3*t**2 + 2*a2*t + a1;
        acc = 6*a3*t + 2*a2;

        return pos, vel

    def move_to_point(self, unit):

        wheel_dir = unit / abs(unit)
        move_period = self.event_period*abs(unit)

        a0x, a1x, a2x, a3x = self.polynomial_coef(self.prev_pos_x, self.pos_x, move_period)
        a0y, a1y, a2y, a3y = self.polynomial_coef(self.prev_pos_y, self.pos_y, move_period)
        self.prev_pos_x = self.pos_x
        self.prev_pos_y = self.pos_y
        t = 0

        while move_period - t >= 0.1:
            posx, velx = self.pos_on_t(t,a0x, a1x, a2x, a3x)
            posy, vely = self.pos_on_t(t,a0y, a1y, a2y, a3y)

            vel = velx
            if vel == 0:
                vel = vely

            t = t + (1 / self.msg_rate)
            self.update(posx, posy, self.dir)
            wheel_distance = ((abs(vel)*wheel_dir) / self.msg_rate)
            for key, value in self.left_wheels.items():
                self.update_wheel(key, value, wheel_distance)
            for key, value in self.right_wheels.items():
                self.update_wheel(key, value, wheel_distance)
            rate = rospy.Rate(self.msg_rate)
            rate.sleep()

    def rotate(self, angle):

        move_period = self.event_period*abs(angle)/(math.pi/2)

        error = self.dir - self.prev_dir;

        if error > math.pi:
            self.prev_dir = self.prev_dir + math.pi*2
        elif error < -math.pi:
            self.prev_dir = self.prev_dir - math.pi*2

        a0, a1, a2, a3 = self.polynomial_coef(self.prev_dir
        , normalize_angle(self.dir), move_period)
        self.prev_dir = self.dir
        t = 0

        while move_period - t >= 0.1:
            pos, vel = self.pos_on_t(t,a0, a1, a2, a3)


            wheel_distance = ((vel*self.wheel_pos*2) / self.msg_rate)
            for key, value in self.left_wheels.items():
                self.update_wheel(key, value, -wheel_distance)
            for key, value in self.right_wheels.items():
                self.update_wheel(key, value, wheel_distance)
            t = t + (1 / self.msg_rate)
            self.update(self.pos_x, self.pos_y, pos)
            rate = rospy.Rate(self.msg_rate)
            rate.sleep()


    def move_distance(self, unit):
        if abs(normalize_angle(self.dir - math.pi/2)) < 0.01:
            self.pos_y += self.unit_length*unit
        elif abs(normalize_angle(self.dir)) < 0.01:
            self.pos_x += self.unit_length*unit
        elif abs(normalize_angle(self.dir - math.pi)) < 0.01:
            self.pos_x -= self.unit_length*unit
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01:
            self.pos_y -= self.unit_length*unit

        if self.blocked(self.pos_x + 5.5, self.pos_y + 4.5):
            print("Labrider: Path is blocked cannot move further")
            self.pos_x = self.prev_pos_x
            self.pos_y = self.prev_pos_y
            return self.pos_x, self.pos_y, self.dir

        self.move_to_point(unit)

        return self.pos_x, self.pos_y, self.dir


    def rotate_angle(self, angle):
        self.dir += angle
        self.dir = normalize_angle(self.dir)

        self.rotate(angle)

        return self.pos_x, self.pos_y, self.dir

    def get_map_heats(self):
        try:
            rospy.wait_for_service("/return_all_heat", 5.0)
            get_heats = rospy.ServiceProxy('return_all_heat', Trigger)
            resp = get_heats()

            map_heats = resp.message.split(":")

            map_heats.remove("")

            self.heat_map = list(map(int, map_heats))

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

    def blocked(self, x, y):
        self.get_map_heats()
        if self.heat_map[int(y)*self.width + int(x)] == -1:
            return True
        else:
            return False
